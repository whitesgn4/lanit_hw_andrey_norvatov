Action()
{
	int iter_step;
	
	lr_start_transaction("UC_Mail_Sending");
	
	web_set_sockets_option("SSL_VERSION", "AUTO");
	
	web_add_auto_header("X-Requested-With", 
		"XMLHttpRequest");	
	
	web_reg_find("Text=Авторизация", 
		LAST);

/*Correlation comment - Do not change!  Original value='a517323774d19a76036f1750149f55853bde0a9e:1622722518477' Name ='csrf_token' Type ='Manual'*/
	web_reg_save_param_attrib(
		"ParamName=csrf_token",
		"TagName=input",
		"Extract=value",
		"Name=csrf_token",
		"Type=hidden",
		SEARCH_FILTERS,
		"IgnoreRedirections=Yes",
		"RequestUrl=*/passport*",
		LAST);

/*Correlation comment - Do not change!  Original value='3b6e422c-2234-4700-953e-e97b9b889f46' Name ='process_uuid' Type ='Manual'*/
	web_reg_save_param_regexp(
		"ParamName=process_uuid",
		"RegExp=process_uuid=(.*?)\"\\ class",
		SEARCH_FILTERS,
		"Scope=Body",
		"IgnoreRedirections=Yes",
		"RequestUrl=*/passport*",
		LAST);

	web_url("lite", 
		"URL=https://mail.yandex.ru/lite", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t287.inf", 
		"Mode=HTML", 
		LAST);

	lr_start_transaction("UC01_authorization");
	
	/*Possible OAUTH authorization was detected. It is recommended to correlate the authorization parameters.*/


/*Correlation comment - Do not change!  Original value='526cf39a466c0a4958f95d8f96d5abbe02' Name ='track_id' Type ='Manual'*/
	web_reg_save_param_json(
		"ParamName=track_id",
		"QueryString=$.track_id",
		SEARCH_FILTERS,
		"Scope=Body",
		"IgnoreRedirections=No",
		LAST);
		

	web_submit_data("start",
		"Action=https://passport.yandex.ru/registration-validations/auth/multi_step/start",
		"Method=POST",
		"TargetFrame=",
		"RecContentType=application/json",
		"Referer=https://passport.yandex.ru/auth?mode=auth&retpath=http%3A%2F%2Fmail.yandex.ru%2Flite",
		"Snapshot=t299.inf",
		"Mode=HTML",
		ITEMDATA,
		"Name=csrf_token", "Value={csrf_token}", ENDITEM,
		"Name=login", "Value={Sender_login}", ENDITEM,
		"Name=process_uuid", "Value={process_uuid}", ENDITEM,
		"Name=retpath", "Value=http://mail.yandex.ru/lite", ENDITEM,
		LAST);


	web_submit_data("commit_password",
		"Action=https://passport.yandex.ru/registration-validations/auth/multi_step/commit_password",
		"Method=POST",
		"TargetFrame=",
		"RecContentType=application/json",
		"Referer=https://passport.yandex.ru/auth/welcome?mode=auth&retpath=http%3A%2F%2Fmail.yandex.ru%2Flite",
		"Snapshot=t304.inf",
		"Mode=HTML",
		ITEMDATA,
		"Name=csrf_token", "Value={csrf_token}", ENDITEM,
		"Name=track_id", "Value={track_id}", ENDITEM,
		"Name=password", "Value={Sender_password}", ENDITEM,
		"Name=retpath", "Value=http://mail.yandex.ru/lite", ENDITEM,
		LAST);

	lr_end_transaction("UC01_authorization",LR_AUTO);

	lr_start_transaction("UC02_send_mail");

/*Correlation comment - Do not change!  Original value='cad500702b1a74ddb3119e078fd7cca5' Name ='compose_check' Type ='Manual'*/
	web_reg_save_param_attrib(
		"ParamName=compose_check",
		"TagName=input",
		"Extract=value",
		"Name=compose_check",
		"Type=hidden",
		SEARCH_FILTERS,
		"IgnoreRedirections=No",
		LAST);

/*Correlation comment - Do not change!  Original value='FRZZ78+ZNHki/uatQfTDF8Y435g=!kpiajyfr' Name ='_ckey' Type ='Manual'*/
	web_reg_save_param_attrib(
		"ParamName=_ckey",
		"TagName=input",
		"Extract=value",
		"Name=_ckey",
		"Type=hidden",
		SEARCH_FILTERS,
		"IgnoreRedirections=No",
		LAST);

	web_url("retpath=", 
		"URL=https://mail.yandex.ru/lite/compose/retpath=", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=https://mail.yandex.ru/lite", 
		"Snapshot=t319.inf", 
		"Mode=HTML", 
		LAST);
	
		
	for(iter_step=0;  iter_step<=2; iter_step++){
	send_mail_iterator();	
	}	
	
	lr_end_transaction("UC02_send_mail",LR_AUTO);
	
	lr_end_transaction("UC_Mail_Sending", LR_AUTO);

	return 0;
}