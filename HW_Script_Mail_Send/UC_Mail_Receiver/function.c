email_selection(){
	web_url("new", 
        "URL=https://mail.yandex.ru/lite/message/{email_random_number}/new", 
        "TargetFrame=", 
        "Resource=0", 
        "RecContentType=text/html", 
        "Referer=https://mail.yandex.ru/lite", 
        "Snapshot=t436.inf", 
        "Mode=HTML", 
        LAST);
};

move_email_to_read_folder(){
	web_submit_data("message-action.xml",
			"Action=https://mail.yandex.ru/lite/message-action.xml",
			"Method=POST",
			"TargetFrame=",
			"RecContentType=text/html",
			"Referer=https://mail.yandex.ru/lite/message/{email_random_number}/new",
			"Snapshot=t446.inf",
			"Mode=HTML",
			ITEMDATA,
			"Name=ids", "Value={email_random_number}", ENDITEM,
			"Name=lids", "Value={lids}", ENDITEM,
			"Name=_handlers", "Value=do-messages", ENDITEM,
			"Name=_ckey", "Value={_ckey_message-action}", ENDITEM,
			"Name=retpath", "Value=message/{email_random_number}", ENDITEM,
			"Name=folder_retpath", "Value=folder/1", ENDITEM,
			"Name=delete_retpath", "Value=folder/1", ENDITEM,
			"Name=more", "Value=Ещё…", ENDITEM,
			"Name=request", "Value=", ENDITEM,
			LAST);    
	
		web_submit_data("message-menu-action.xml",
			"Action=https://mail.yandex.ru/lite/message-menu-action.xml",
			"Method=POST",
			"TargetFrame=",
			"RecContentType=text/html",
			"Referer=https://mail.yandex.ru/lite/message-menu?retpath=message%2F{email_random_number}&delete_retpath=folder%2F1&count=1&ids={email_random_number}&folder_retpath=folder%2F1&lids={lids_URL2}&",
			"Snapshot=t459.inf",
			"Mode=HTML",
			ITEMDATA,
			"Name=_ckey", "Value={_ckey_message-menu-action}", ENDITEM,
			"Name=ids", "Value={email_random_number}", ENDITEM,
			"Name=_handlers", "Value=do-messages", ENDITEM,
			"Name=retpath", "Value=message/{email_random_number}", ENDITEM,
			"Name=count", "Value=1", ENDITEM,
			"Name=folder_retpath", "Value=folder/1", ENDITEM,
			"Name=delete_retpath", "Value=folder/1", ENDITEM,
			"Name=movefile", "Value=7", ENDITEM,
			"Name=lid", "Value=", ENDITEM,
			"Name=unlid", "Value=", ENDITEM,
			"Name=move", "Value=Выполнить", ENDITEM,
			LAST);

		
}