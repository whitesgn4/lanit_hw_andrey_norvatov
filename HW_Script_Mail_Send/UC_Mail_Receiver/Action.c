Action()
{
	lr_start_transaction("UC_Mail_Receiving");

	
	web_set_sockets_option("SSL_VERSION", "AUTO");
	
	web_add_auto_header("X-Requested-With", 
		"XMLHttpRequest");
	
	web_reg_find("Text=Авторизация", 
	    LAST);

/*Correlation comment - Do not change!  Original value='5d7d337f7ed24fb660b75cb6f73100ccc94d170f:1622792809790' Name ='csrf_token' Type ='Manual'*/
	web_reg_save_param_attrib(
	    "ParamName=csrf_token",
	    "TagName=input",
	    "Extract=value",
	    "Name=csrf_token",
	    "Type=hidden",
	    SEARCH_FILTERS,
	    "IgnoreRedirections=Yes",
	    "RequestUrl=*/passport*",
	    LAST);

/*Correlation comment - Do not change!  Original value='7addda0c-73ce-4f7b-8c34-1042e623fe26' Name ='process_uuid' Type ='Manual'*/
	web_reg_save_param_regexp(
	    "ParamName=process_uuid",
	    "RegExp=process_uuid=(.*?)\"\\ class",
	    SEARCH_FILTERS,
	    "Scope=Body",
	    "IgnoreRedirections=Yes",
	    "RequestUrl=*/passport*",
	    LAST);

	web_url("lite", 
	    "URL=https://mail.yandex.ru/lite", 
	    "TargetFrame=", 
	    "Resource=0", 
	    "RecContentType=text/html", 
	    "Referer=", 
	    "Snapshot=t407.inf", 
	    "Mode=HTML", 
	    LAST);

	lr_start_transaction("UC01_authorization");	

/*Possible OAUTH authorization was detected. It is recommended to correlate the authorization parameters.*/


/*Correlation comment - Do not change!  Original value='d03d5883f8ce5a93ef66761bc61280830b' Name ='track_id' Type ='Manual'*/
    web_reg_save_param_json(
        "ParamName=track_id",
        "QueryString=$.track_id",
        SEARCH_FILTERS,
        "Scope=Body",
        "IgnoreRedirections=No",
        "RequestUrl=*/start*",
        LAST);

    web_submit_data("start",
        "Action=https://passport.yandex.ru/registration-validations/auth/multi_step/start",
        "Method=POST",
        "TargetFrame=",
        "RecContentType=application/json",
        "Referer=https://passport.yandex.ru/auth?mode=auth&retpath=http%3A%2F%2Fmail.yandex.ru%2Flite",
        "Snapshot=t416.inf",
        "Mode=HTML",
        ITEMDATA,
        "Name=csrf_token", "Value={csrf_token}", ENDITEM,
        "Name=login", "Value={Reciver_login}", ENDITEM,
        "Name=process_uuid", "Value={process_uuid}", ENDITEM,
        "Name=retpath", "Value=http://mail.yandex.ru/lite", ENDITEM,
        LAST);

    web_submit_data("commit_password",
        "Action=https://passport.yandex.ru/registration-validations/auth/multi_step/commit_password",
        "Method=POST",
        "TargetFrame=",
        "RecContentType=application/json",
        "Referer=https://passport.yandex.ru/auth/welcome?mode=auth&retpath=http%3A%2F%2Fmail.yandex.ru%2Flite",
        "Snapshot=t421.inf",
        "Mode=HTML",
        ITEMDATA,
        "Name=csrf_token", "Value={csrf_token}", ENDITEM,
        "Name=track_id", "Value={track_id}", ENDITEM,
        "Name=password", "Value={Reciver_password}", ENDITEM,
        "Name=retpath", "Value=http://mail.yandex.ru/lite", ENDITEM,
        LAST);

    lr_end_transaction("UC01_authorization",LR_AUTO);    
 
    
    
/*Correlation comment - Do not change!  Original value='176203335420870701' Name ='' Type ='Manual'*/    
	
	 web_reg_save_param_regexp(
		"ParamName=email_number",
		"RegExp=(a href=\"\\/lite\\/message\\/)(\\d*)",
		"NotFound=warning",
		"Group=2",
		"Ordinal=all",
		SEARCH_FILTERS,
		LAST); 

	
	web_url("lite_2",
		"URL=https://mail.yandex.ru/lite", 
		"TargetFrame=", 
		"Resource=0", 
		"RecContentType=text/html", 
		"Referer=", 
		"Snapshot=t507.inf", 
		"Mode=HTML", 
		LAST);
	
//	lr_output_message( "ТЕКСТ #%s", lr_eval_string( "{email_number_count}" ) );
	
		
	if ( atoi(lr_eval_string("{email_number_count}")) == 0 ){
 		lr_output_message( "Your inbox is empty." );
	}
	else{
		
	lr_start_transaction("UC02_email_selection");
	
	lr_save_string(lr_paramarr_random("email_number"), "email_random_number");

/*Correlation comment - Do not change!  Original value='KxxkeTNtmKkAxNbrSbYaRGXS6Ls=!kpjgeb49' Name ='_ckey_message-action' Type ='Manual'*/
    web_reg_save_param_attrib(
        "ParamName=_ckey_message-action",
        "TagName=input",
        "Extract=value",
        "Name=_ckey",
        "Type=hidden",
        SEARCH_FILTERS,
        "IgnoreRedirections=No",
        LAST);    


/*Correlation comment - Do not change!  Original value='12,14,15,FAKE_MULCA_SHARED_LBL,FAKE_RECENT_LBL,FAKE_SEEN_LBL' Name ='lids' Type ='Manual'*/
	web_reg_save_param_attrib(
		"ParamName=lids",
		"TagName=input",
		"Extract=value",
		"Name=lids",
		"Type=hidden",
		SEARCH_FILTERS,
		"IgnoreRedirections=No",
		LAST);

	email_selection();

	web_convert_param("lids_URL2",
		"SourceString={lids}",
		"SourceEncoding=HTML",
		"TargetEncoding=URL",
		LAST);       
	
  
/*Correlation comment - Do not change!  Original value='eBkyiJ6gf1dmz4L97vMM8mG21yc=!kpjgehty' Name ='_ckey_message-menu-action' Type ='Manual'*/
    web_reg_save_param_attrib(
        "ParamName=_ckey_message-menu-action",
        "TagName=input",
        "Extract=value",
        "Name=_ckey",
        "Type=hidden",
        SEARCH_FILTERS,
        "IgnoreRedirections=Yes",
        "RequestUrl=*/message-menu*",
        LAST);
                
	lr_end_transaction("UC02_email_selection",LR_AUTO);	
	
	lr_start_transaction("UC03_move_email_to_read_folder");

	move_email_to_read_folder();
    
    lr_end_transaction("UC03_move_email_to_read_folder",LR_AUTO);

	
    lr_start_transaction("UC04_back_to_inbox");

    
    web_url("inbox",
		"URL=https://mail.yandex.ru/lite/inbox",
		"TargetFrame=",
		"Resource=0",
		"RecContentType=text/html",
		"Snapshot=t470.inf",
		"Mode=HTML",
		LAST);    

    lr_end_transaction("UC04_back_to_inbox",LR_AUTO);
	}
	
	lr_end_transaction("UC_Mail_Receiving", LR_AUTO);

	
	return 0;
}